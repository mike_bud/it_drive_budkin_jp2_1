package ru.itdrive.information.app;

import java.io.File;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
class Arguments{

@Parameter(names = "--file")
String file;

}