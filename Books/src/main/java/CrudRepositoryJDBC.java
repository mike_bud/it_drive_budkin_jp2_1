import java.util.List;

public interface CrudRepositoriesJDBS <T> {
    void save(T obj);
    void update(T obj);
    void delete(Integer id);
    T find(Integer id) throws Exception;
    List<T> findAll() throws Exception;

}
