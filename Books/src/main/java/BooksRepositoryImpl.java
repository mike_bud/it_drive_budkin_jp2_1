import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookRepositoriesImpl implements BooksRepository {

    //Language=SQL
    private static final String SQL_FIND_BY_ID ="select * from book where id = ? ";

    //Language=SQL
    private static final String SQL_FIND_BY_TITLE ="select * from book where title = ? ";

    //Language=SQL
    private static final String SQL_DELETE_BY_ID ="delete from book where id = ? ";

    //Language=SQL
    private static final String SQL_ADD_BOOK ="insert into book (id,title, author, color) values (?,?,?,?)  ";

    //Language=SQL
    private static final String SQL_UPDATE_TITLE ="update book set title = ? where id = ? ";

    //Language=SQL
    private static final String SQL_SELECT_ALL_BOOKS ="select  * from book";


    Connection connection;

    public BookRepositoriesImpl(Connection connection) {
        this.connection = connection;
    }

    private RowMapper<Book> bookRowMapper = new RowMapper<Book>() {
        public Book mapRow(ResultSet row) throws SQLException {
            return new Book(
                    row.getInt("id"),
                    row.getString("title"),
                    row.getString("author"),
                    row.getString("color")
            );
        }
    };

    public Book findByTitle(String title) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_TITLE);
        preparedStatement.setString(1, title);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        return bookRowMapper.mapRow(rs);
    }

    public void save(Book obj) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_ADD_BOOK);
            statement.setInt(1, obj.getId());
            statement.setString(2, obj.getTitle());
            statement.setString(3, obj.getAuthor());
            statement.setString(4, obj.getColor());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void update(Book obj){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_TITLE);
            preparedStatement.setString(1, obj.getTitle());
            preparedStatement.setInt(2, obj.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void delete(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID);
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Book find(Integer id) throws Exception {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            return bookRowMapper.mapRow(rs);

    }

    public List<Book> findAll() throws Exception{

        List<Book> books = new ArrayList<Book>();
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_BOOKS);
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()){
            Book book =bookRowMapper.mapRow(rs);
            books.add(book);
        }
        return books;
    }



}
