public interface BookRepositories extends CrudRepositoriesJDBS <Book>{
            Book findByTitle(String title) throws Exception;
}
