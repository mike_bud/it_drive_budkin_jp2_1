import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final String URL = "jdbc:postgresql://localhost:5432/bookmagazin";
    private static final String USER = "postgres";
    private static final String PASSWORD = "admin";

    public static void main(String[] args) throws Exception {

        Connection connection = DriverManager.getConnection(URL,USER,PASSWORD);

        BookRepositories bookRepositories = new BookRepositoriesImpl(connection);

//        bookRepositories.delete(4);    // УДАЛЕНИЕ ПО ID

//        System.out.println("------------------------------------------------------------------");

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Введите id книги");
//        String idd = scanner.nextLine();
//        int id = Integer.parseInt(idd);
//        System.out.println("Введите название книги");
//        String title = scanner.nextLine();
//        System.out.println("Введите автора книги");       // ДОБАВЛЕНИЕ
//        String author = scanner.nextLine();
//        System.out.println("Введите цвет книги");
//        String color = scanner.nextLine();
//
//        Book book = Book.builder()
//                .id(id)
//                .author(author)
//                .color(color)
//                .title(title)
//                .build();
//        System.out.println(book);
//        bookRepositories.save(book);

//        System.out.println("------------------------------------------------------------------");

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Введите id книги");
//        String idd = scanner.nextLine();
//        int id = Integer.parseInt(idd);                //  ПОИСК ПО ID
//
//        Book book = bookRepositories.find(id);
//        System.out.println(book);

//        System.out.println("------------------------------------------------------------------");

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Поиск");
//        System.out.println("Введите название книги");          // ПОИСК ПО НАЗВАНИЮ
//        String title = scanner.nextLine();
//
//        Book book = bookRepositories.findByTitle(title);
//        System.out.println(book);

//        System.out.println("------------------------------------------------------------------");

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Введите новое название Книги");
//        String title = scanner.nextLine();
//        System.out.println("Изменяю книгу под id");
//        String idd = scanner.nextLine();                      // ОБНОВЛЯЕМ НАЗВАНИЕ КНИГИ
//        int id = Integer.parseInt(idd);
//
//        Book book = Book.builder()
//                .title(title)
//                .id(id)
//                .build();
//        bookRepositories.update(book);

//        System.out.println("------------------------------------------------------------------");

        List<Book> books = bookRepositories.findAll();
        System.out.println(books);                              // ВЫВОД ВСЕХ КНИГ


    }
}
