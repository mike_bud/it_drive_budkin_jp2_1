package ru.itdrive.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Message {
    private Integer id;
    private String  text;
    private String  nick;
    private Integer room;

    public Message(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return ("\t" + nick + ": " +  text + "\n")
                .replace("[", "")
                .replace("]", "");
    }
}
