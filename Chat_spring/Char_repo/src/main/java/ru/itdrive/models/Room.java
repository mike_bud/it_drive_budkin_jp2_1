package ru.itdrive.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Room   {
    private Integer id;
    private List<Message> messageList;
    private List<User> userList;

    public Room(Integer id) {
        this.id = id;
    }

    public Room(Integer id, List<Message> messageList) {
        this.id = id;
        this.messageList = messageList;
    }
    @Override
    public String toString() {
        return ("Room " + id + "\n" + messageList)
                .replace("[", "")
                .replace("]", "")
                .replace(",", "");
    }
}
