package ru.itdrive.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class User {
    private Integer id;
    private String nickName;

    public User(String nickName) {
        this.nickName = nickName;
    }

    private List<Room> roomList;
    private List<Message> messageList;
}
