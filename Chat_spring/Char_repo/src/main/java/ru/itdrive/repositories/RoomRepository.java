package ru.itdrive.repositories;

import ru.itdrive.models.Room;
import ru.itdrive.repositories.CrudRepositoryJDBC;

public interface RoomRepository extends CrudRepositoryJDBC<Room> {

}
