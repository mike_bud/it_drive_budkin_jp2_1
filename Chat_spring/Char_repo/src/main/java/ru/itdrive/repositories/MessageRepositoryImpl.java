package ru.itdrive.repositories;

import ru.itdrive.models.Message;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageRepositoryImpl implements MessageRepository {

    //Language=SQL
    private static final String SQL_ADD_MESSAGE = "insert into messages (text, nick, id_room) VALUES (?,?,?)";
    //Language=SQL
    private static final String SQL_ADD_MESSAGE_IN_ROOM = "insert into rooms_messages (id_message, id_room) VALUES (?,?)";  // КАК СДЕЛАТЬ ПРОЩЕ?
    //Language=SQL
    public static final String SQL_FIND_MESSAGE_BY_ID = "select * from message where id = ?";
    //Language=SQL
    public static final String SQL_DELETE_MESSAGE_BY_ID = "delete from message where id = ?";
    //Language=SQL
    public static final String SQL_FIND_ALL_MESSAGE = "select * from message";
    //Language=SQL
    private static final String SQL_UPDATE_MESSAGE = "update message set text = ? where id = ? ";

    private DataSource dataSource;

    public MessageRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Message> messageRowMapper = new RowMapper<Message>() {
        @Override
        public Message mapRow(ResultSet row) throws SQLException {
            return new Message (
                    row.getString("text"));
        }
    };
    @Override
    public void add(Message object) {
        try {
            Connection connection =dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_MESSAGE);
            preparedStatement.setString(1,object.getText());
            preparedStatement.setString(2,object.getNick());
            preparedStatement.setInt(3, object.getRoom());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    @Override
    public void addMessageInRoom(Message object) {
        try {

            Connection connection =dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_MESSAGE_IN_ROOM);
            preparedStatement.setInt(1, object.getId());
            preparedStatement.setInt(2,object.getRoom());
            preparedStatement.executeUpdate();
            preparedStatement.close();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }
    @Override
    public void update(Message object) {
        try {
            Connection connection =dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_MESSAGE);
            preparedStatement.setInt(1, object.getId());
            preparedStatement.setInt(2,object.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    @Override
    public void delete(Integer id) {
        try {
            Connection connection =dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_MESSAGE_BY_ID);
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    @Override
    public Message find(Integer id) {
        try {
            Connection connection =dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_MESSAGE_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return messageRowMapper.mapRow(resultSet);

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    @Override
    public List<Message> findAll() {
        try {
            List<Message> list = new ArrayList<>();

            Connection connection =dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_MESSAGE);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Message message = messageRowMapper.mapRow(resultSet);
                list.add(message);
            }
            return list;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
