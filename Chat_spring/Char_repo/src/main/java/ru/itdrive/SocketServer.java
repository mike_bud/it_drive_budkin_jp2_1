package ru.itdrive;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itdrive.models.Message;
import ru.itdrive.models.Room;
import ru.itdrive.models.User;
import ru.itdrive.repositories.MessageRepository;
import ru.itdrive.repositories.RoomRepository;
import ru.itdrive.repositories.UserRepository;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SocketServer {

    List<ChatSocketClient> anonyms = new ArrayList<>();
//    Map<Long, List<ChatSocketClient>> map;
//    int id = Integer.parseInt(parsedCommand[2]);
//    Room room =  new Room(id,new ArrayList<>());
//    map.put(id,room)
//    Message message = Message.builder()
//        .nick("nick")
//        .text("text")
//        .build();
//    map.get(id).getMessageList().add(message);





//    Map<Integer, User> map;
//    MessagesService  messagesService;


    ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
    RoomRepository roomRepository = context.getBean("RoomRepositoryImpl", RoomRepository.class);
    UserRepository userRepository = context.getBean("UserRepositoryImpl", UserRepository.class);
    MessageRepository messageRepository = context.getBean("MessageRepositoryImpl", MessageRepository.class);

    public void start(int port) {
        ServerSocket serverSocket;
            try {
            serverSocket = new ServerSocket(port);
            while (true) {
                Socket socketClient = serverSocket.accept();
                ChatSocketClient clientHandler = new ChatSocketClient(socketClient);
                clientHandler.start();
                anonyms.add(clientHandler);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public class ChatSocketClient extends Thread {
        

        private Socket socketClient;

        public ChatSocketClient(Socket socketClient) {
            this.socketClient = socketClient;
        }

        @Override
        public void run() {
            InputStream clientinputstream = null;
            try {
                clientinputstream = socketClient.getInputStream();
                BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientinputstream));
                String inputLine = clientReader.readLine();
                while (inputLine != null) {
                    List<User> users = userRepository.findAll();
                    String parsedCommand[] = inputLine.split(" ");
                    if (parsedCommand[0].equals("choose") && parsedCommand[1].equals("room")) {
                        int id = Integer.parseInt(parsedCommand[2]);
                        Room room = roomRepository.find(id);
                        PrintWriter writer = new PrintWriter(socketClient.getOutputStream(), true);
                        writer.println(room.toString());

                        inputLine = clientReader.readLine();
                        String parsedCommand1[] = inputLine.split(" ");

                        if (parsedCommand1[0].equals("user")) {
                            String name = parsedCommand1[1];
                            User user = User.builder()
                                    .nickName(name)
                                    .build();
                            if (!users.contains(user))
                                userRepository.add(user);

                            inputLine = clientReader.readLine();
                            while (!inputLine.equals("exit")) {
                                Message message = Message.builder()
                                        .room(id)
                                        .text(inputLine)
                                        .nick(name)
                                        .build();
                                messageRepository.add(message);
                                for (ChatSocketClient clientHandler : anonyms) {
                                    PrintWriter writer1 = new PrintWriter(clientHandler.socketClient.getOutputStream(), true);
                                    writer1.println(message.toString());
                                }
                                inputLine = clientReader.readLine();
                            }
                            continue;
                        }
                        continue;
                    }
                    inputLine = clientReader.readLine();
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
