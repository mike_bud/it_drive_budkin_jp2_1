package ru.itdrive;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ChatSocketClient {

    private Socket client;

    private PrintWriter towriterServer;
    private BufferedReader fromreaderServer;

    public ChatSocketClient(String host, int port) {
        try {
            client = new Socket(host, port);
            towriterServer = new PrintWriter(client.getOutputStream(), true);
            fromreaderServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
            new Thread(receiveMessageTask).start();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }

    public void sendMessage(String message) {
        towriterServer.println(message);
    }

    private Runnable receiveMessageTask = new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    String messageFromServer = fromreaderServer.readLine();
                    if (messageFromServer != null) {
                        System.out.println(messageFromServer);
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    };
}
