package ru.itdrive;

import com.beust.jcommander.JCommander;

import java.sql.SQLException;
import java.util.Scanner;

public class MainForClient {

    public static void main(String[] args) throws SQLException {
        Arguments argument = new Arguments ();

        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);


        Scanner scanner = new Scanner(System.in);

        ChatSocketClient chatSocketClient = new ChatSocketClient("127.0.0.1", 8000);

        while (true) {
            String text = scanner.nextLine();
            chatSocketClient.sendMessage(text);

        }
   }
}
