package ru.itdrive;

import com.beust.jcommander.JCommander;

import java.sql.SQLException;

public class MainForServer {

    public static void main(String[] args) throws SQLException {
        Arguments argument = new Arguments ();

        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);


        SocketServer socketServer = new SocketServer();
        socketServer.start(8000);
    }
}
