package ru.itdrive;

import lombok.SneakyThrows;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itdrive.models.Message;
import ru.itdrive.models.Room;
import ru.itdrive.models.User;
import ru.itdrive.services.MessagesService;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SocketServer {

    private List<ChatSocketClient> anonyms;

    private Map<Integer, List<ChatSocketClient>> usersInRooms;

    private MessagesService messagesService;

    public void start(int port) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        messagesService = context.getBean(MessagesService.class);

        this.anonyms = new ArrayList<>();
        this.usersInRooms = new HashMap<>();

        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                Socket socketClient = serverSocket.accept();
                ChatSocketClient clientHandler = new ChatSocketClient(socketClient);
                clientHandler.start();
                anonyms.add(clientHandler);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public class ChatSocketClient extends Thread {

        private Integer userNick;
        private Integer roomId;
        private BufferedReader input;
        private PrintWriter output;

        public ChatSocketClient(Socket socketClient) {
            try {
                this.input = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
                this.output = new PrintWriter(socketClient.getOutputStream(), true);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        @SneakyThrows  // lombok exception
        public void run() {
            String inputLine = input.readLine();
            while (inputLine != null) {
                List<User> users = messagesService.findAllUsers();
                String[] parsedCommand = inputLine.split(" ");
                if (parsedCommand[0].equals("choose") && parsedCommand[1].equals("room")) {
                    roomId = Integer.parseInt(parsedCommand[2]);
                    Room room = messagesService.findRoom(roomId);
                    output.println(room.toString());

                    if (!usersInRooms.containsKey(roomId)) {
                        usersInRooms.put(roomId, new ArrayList<>());
                    }

                    usersInRooms.get(roomId).add(this);

                    inputLine = input.readLine();
                    parsedCommand = inputLine.split(" ");

                    if (parsedCommand[0].equals("user")) {
                        String userNick = parsedCommand[1];
                        User user = User.builder()
                                .nickName(userNick)
                                .build();
                        if (!users.contains(user))
                            messagesService.addUser(user);

                        inputLine = input.readLine();
                        while (!inputLine.equals("exit")) {
                            Message message = Message.builder()
                                    .room(roomId)
                                    .text(inputLine)
                                    .nick(userNick)
                                    .build();
                            messagesService.addMessage(message);

                            for (ChatSocketClient client : usersInRooms.get(roomId)) {
                                PrintWriter writer = new PrintWriter(client.output, true);
                                writer.println(message);
                            }
                            inputLine = input.readLine();
                        }
                        continue;
                    }
                    continue;
                }
                inputLine = input.readLine();
            }
        }
    }
}
