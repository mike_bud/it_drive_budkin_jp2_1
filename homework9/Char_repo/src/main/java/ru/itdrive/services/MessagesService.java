package ru.itdrive.services;

import ru.itdrive.models.Message;
import ru.itdrive.models.Room;
import ru.itdrive.models.User;

import java.util.List;

public interface MessagesService {
    List<User> findAllUsers();

    Room findRoom(int id);

    void addUser(User user);

    void addMessage(Message message);
}
