package ru.itdrive.services;

import ru.itdrive.models.Message;
import ru.itdrive.models.Room;
import ru.itdrive.models.User;
import ru.itdrive.repositories.MessageRepository;
import ru.itdrive.repositories.RoomRepository;
import ru.itdrive.repositories.UserRepository;

import java.util.List;

public class MessagesServiceImpl implements MessagesService {
    private RoomRepository roomRepository;
    private UserRepository userRepository;
    private MessageRepository messageRepository;

    public MessagesServiceImpl(RoomRepository roomRepository, UserRepository userRepository, MessageRepository messageRepository) {
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
    }


    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Room findRoom(int id) {
        return roomRepository.find(id);
    }

    @Override
    public void addUser(User user) {
        userRepository.add(user);
    }

    @Override
    public void addMessage(Message message) {
        messageRepository.add(message);
    }
}
