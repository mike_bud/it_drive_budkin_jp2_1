package ru.itdrive.repositories;

import ru.itdrive.models.User;
import ru.itdrive.repositories.CrudRepositoryJDBC;

public interface UserRepository extends CrudRepositoryJDBC<User> {

}
