package ru.itdrive.repositories;

import ru.itdrive.models.Message;
import ru.itdrive.repositories.CrudRepositoryJDBC;

public interface MessageRepository extends CrudRepositoryJDBC<Message> {
 void addMessageInRoom (Message object);
}
