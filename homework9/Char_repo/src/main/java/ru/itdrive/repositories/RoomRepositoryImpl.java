package ru.itdrive.repositories;

import ru.itdrive.models.Message;
import ru.itdrive.models.Room;
import ru.itdrive.repositories.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoomRepositoryImpl implements RoomRepository {

    //Language=SQL
    private static final String SQL_ADD_USER_IN_ROOM = "insert into users (nicknam) values (?)";

    //Language=SQL
    public static final String SQL_FIND_ROOM_BY_ID = "select * from rooms where id = ?";

    //Language=SQL
    public static final String SQL_DELETE_ROOM_BY_ID = "delete from rooms where id = ?";

    //Language=SQL
    public static final String SQL_FIND_ALL_ROOMS = "select * from rooms";

    //Language=SQL
    private static final String SQL_UPDATE_ROOM = "update rooms set numroom = ? where id = ? ";

    //Language=SQL
    public static final String SQL_SHOW_ALL_MESSAGES_IN_ROOM = "select rooms.id, nick, text from rooms\n" +
            "join messages m on rooms.id = m.id_room\n" +
            "where rooms.id=?;";

//    //Language=SQL
//    public static final String SQL_SHOW_ALL_MESSAGES_IN_ROOMM = "SELECT * from rooms\n" +
//            "join rooms_messages rm on rooms.id = rm.id_room\n" +
//            "join messages m on rm.id_message = m.id";

    DataSource dataSource;

    public RoomRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        @Override
        public Room mapRow(ResultSet row) throws SQLException {
            return new Room (row.getInt("id"));
        }
    };


    private Map<Integer, Room> map = new HashMap<>();

    private RowMapper<Room> roomWithMessagesRowMapper = new RowMapper<Room>() {
        @Override
        public Room mapRow(ResultSet row) throws SQLException {
            Integer roomId = row.getInt("id");
            if (!map.containsKey(roomId)) {
                Room room = new Room(row.getInt("id"), new ArrayList<>());
                map.put(roomId, room);
            }
            Message message = Message.builder()
                    .nick(row.getString("nick"))
                    .text(row.getString("text"))
                    .build();
            map.get(roomId).getMessageList().add(message);

            return null;
        }
    };

    @Override
    public void add(Room object) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_USER_IN_ROOM);
            preparedStatement.setInt(1,object.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Room object) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_ROOM);
            preparedStatement.setInt(1, object.getId());
            preparedStatement.setInt(2,object.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void delete(Integer id) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_ROOM_BY_ID);
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Room find(Integer id) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_ALL_MESSAGES_IN_ROOM);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                roomWithMessagesRowMapper.mapRow(resultSet);
            }
            Room room = map.get(id);
            map.clear();
            return room;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Room> findAll() {
        try {
            List<Room> list = new ArrayList<>();

            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_ROOMS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Room room = roomRowMapper.mapRow(resultSet);
                list.add(room);
            }
            return list;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

//    @Override
//    public List<Room> findAlll() {
//        try {
//            // список курсов
//            List<Room> result = new ArrayList<Room>();
//            Statement statement = connection.createStatement();
//            ResultSet resultSet = statement.executeQuery(SQL_SHOW_ALL_MESSAGES_IN_ROOMM);
//            // мапа - ключ - название курса, значение - список название предметов
//
//            while (resultSet.next()){
//                aauthorRowMapper.mapRow(resultSet);
//            }
//            for (Map.Entry<Integer,Room> courseEntry: map.entrySet()){
//                result.add(new Room(courseEntry.getValue().getId(), courseEntry.getValue().getMessageList()));
//            }
//
//            return result;
//
//        } catch (SQLException e) {
//            throw new IllegalArgumentException(e);
//        }
//    }


//    @Override
//    public List<Room> findd(Integer id) {
//        try {
//            List<Room> result = new ArrayList<Room>();
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_ALL_MESSAGES_IN_ROOM);
//            preparedStatement.setInt(1,id);
//            ResultSet resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()){
//                aauthorRowMapper.mapRow(resultSet);
//            }
//            for (Map.Entry<Integer,Room> courseEntry: map.entrySet()){
//                result.add(new Room(courseEntry.getValue().getId(), courseEntry.getValue().getMessageList()));
//            }
//            return result;
//
//        } catch (SQLException e) {
//            throw new IllegalArgumentException(e);
//        }
//    }

}
