package ru.itdrive.repositories;

import ru.itdrive.models.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {

    //Language=SQL
    private static final String SQL_ADD_USER = "insert into users (nickname) values (?)";
    //Language=SQL
    public static final String SQL_FIND_USER_BY_ID = "select * from users where id = ?";
    //Language=SQL
    public static final String SQL_DELETE_USER_BY_ID = "delete  from users where id = ?";
    //Language=SQL
    public static final String SQL_FIND_ALL_USERS = "select * from users";
    //Language=SQL
    private static final String SQL_UPDATE_USER = "update users set nickname = ? where id = ? ";

    private DataSource dataSource;

    public UserRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

        private RowMapper<User> userRowMapper = new RowMapper<User>() {
        @Override
        public User mapRow(ResultSet row) throws SQLException {
            return new User(row.getString("nickname"));
        }
    };
    @Override
    public void add(User object) {
        try {
            Connection connection =dataSource.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_USER);
            preparedStatement.setString(1,object.getNickName());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    @Override
    public void update(User object) {
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_USER);
//            preparedStatement.setString(1, object.getName());
//            preparedStatement.setInt(2,object.getId());
//            preparedStatement.executeUpdate();
//            preparedStatement.close();
//        } catch (SQLException e) {
//            throw new IllegalArgumentException(e);
//        }
        }

    @Override
    public void delete(Integer id) {
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER_BY_ID);
//            preparedStatement.setInt(1,id);
//            preparedStatement.executeUpdate();
//            preparedStatement.close();
//        } catch (SQLException e) {
//            throw new IllegalArgumentException(e);
//        }

    }

    @Override
    public User find(Integer id) {
//            try {
//                PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_USER_BY_ID);
//                preparedStatement.setInt(1, id);
//                ResultSet resultSet = preparedStatement.executeQuery();
//                resultSet.next();
//                return userRowMapper.mapRow(resultSet);
//
//            } catch (SQLException e) {
//                throw new IllegalArgumentException(e);
//            }
        return null;
    }

    @Override
    public List<User> findAll() {
        try {
            List<User> list = new ArrayList<>();
            Connection connection =dataSource.getConnection();


            PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_USERS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
               User user = userRowMapper.mapRow(resultSet);
               list.add(user);
            }
            return list;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
