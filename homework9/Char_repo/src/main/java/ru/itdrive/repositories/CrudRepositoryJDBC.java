package ru.itdrive.repositories;

import java.util.List;

public interface CrudRepositoryJDBC<T> {
    void add(T object);
    void update(T object);
    void delete(Integer id);
    T find(Integer id);

    List<T> findAll();
}
