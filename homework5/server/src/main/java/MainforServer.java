import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


import com.beust.jcommander.JCommander;

public class MainforServer {

    public static void main(String[] args) throws IOException {
        Arguments argument = new Arguments ();

        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);


        ServerSocket serverSocket = new ServerSocket(argument.port);

        while (true){
            Socket socket  = serverSocket.accept();
            ClientHandler handler = new ClientHandler(socket);
            handler.start();
        }
    }
}
