package ru.itdrive;

import java.util.Scanner;

import com.beust.jcommander.JCommander;

public class MainforClient {
    public static void main(String[] args) {

        Arguments argument = new Arguments ();

        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);


        Scanner scanner = new Scanner(System.in);

        Client client = new Client(argument.host, argument.port);

        while(true){
            String message = scanner.nextLine();
            client.sendMessage(message);
        }
    }
}
