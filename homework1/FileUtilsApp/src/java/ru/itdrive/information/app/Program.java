package ru.itdrive.information.app;
import java.io.File;

import com.beust.jcommander.JCommander;
import ru.itdrive.information.utils.FileInformationApi; 


class Program{
	public static void main(String[] args) {
		Arguments argument = new Arguments ();

	JCommander.newBuilder()
		.addObject(argument)
		.build()
		.parse(args);

		FileInformationApi inf = new FileInformationApi ();
		
		inf.getFilesInformation(new File(argument.file));
	}
}