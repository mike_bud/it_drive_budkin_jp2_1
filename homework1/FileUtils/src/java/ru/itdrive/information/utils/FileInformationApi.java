package ru.itdrive.information.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileInformationApi extends FileInformation {

    public List<FileInformation> getFilesInformation(File folder) {

        List<FileInformation> list = new ArrayList<>();
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println(listOfFiles[i].getName() + " \t File ");
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println(listOfFiles[i].getName() + " \t Directory ");
            }
        }
        return list;
    }
}