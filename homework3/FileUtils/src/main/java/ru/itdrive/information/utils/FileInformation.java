package ru.itdrive.information.utils;


public class FileInformation {

    String fileName;
    long fileSize;

    public FileInformation() {
    }

    public FileInformation(String fileName, long fileSize) {
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
}

//        System.out.println("Поток: " + Thread.currentThread().getName() + ". Папка: " + folder.getName());
