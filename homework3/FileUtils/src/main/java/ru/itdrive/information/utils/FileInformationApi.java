package ru.itdrive.information.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;


public class FileInformationApi extends FileInformation {

    public Map <String, List<FileInformation>> getFilesInformation(File folder) {

        Map <String, List<FileInformation>> map = new HashMap<>();

        List<FileInformation> list = new ArrayList<>();

        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
               list.add(new FileInformation(listOfFiles[i].getName(), listOfFiles[i].length()));
            } else if (listOfFiles[i].isDirectory()) {
                list.add(new FileInformation(listOfFiles[i].getName(), listOfFiles[i].length()));
            }
        }

        map.put(folder.getName(), list);

        return map;
    }
}