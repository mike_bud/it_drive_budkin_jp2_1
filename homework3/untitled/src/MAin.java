import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MAin {
    public static void main(String[] args) {
        Animal cat = new Animal("Marsel", 10);
        Animal dog = new Animal("Charlik", 8);
        List<Animal> animals = new ArrayList<>();
        animals.add(cat);
        animals.add(dog);

        Map<String, List<Animal>> map = new HashMap<>();
        map.put("1 == ", animals);
        map.put("2 == ", animals);

        for(Map.Entry<String, List<Animal>> pair : map.entrySet()){
            System.out.println(pair.getKey() + " - " + pair.getValue());
        }

//        System.out.println(map);
    }

}
