package ru.itdrive.information.app;

import java.io.File;
import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Arguments {
    @Parameter(names = "--files", splitter = Splitter.class)
    List<String> files;

    public List<String> getFiles() {
        return files;
    }
}
