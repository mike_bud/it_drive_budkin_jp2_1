package ru.itdrive.information.app;


import com.beust.jcommander.converters.IParameterSplitter;

import java.util.Arrays;
import java.util.List;

public class Splitter implements IParameterSplitter {
    public List<String> split(String value) {
        return Arrays.asList(value.split(";"));
    }
}
