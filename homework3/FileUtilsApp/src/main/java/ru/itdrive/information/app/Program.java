package ru.itdrive.information.app;

import java.io.File;

import com.beust.jcommander.JCommander;
import ru.itdrive.information.utils.FileInformation;
import ru.itdrive.information.utils.FileInformationApi;
import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


class Program {
	public static void main(String[] args) {
		Arguments argument = new Arguments();
		final FileInformationApi api = new FileInformationApi();


		JCommander.newBuilder()
				.addObject(argument)
				.build()
				.parse(args);


		List<String> files = argument.getFiles();
		for (final String folderName : files) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					Map<String,List<FileInformation>> folders = api.getFilesInformation(new File(folderName));
					for (Map.Entry<String , List<FileInformation>> entry : folders.entrySet()){
						System.out.println("Папка: " + entry.getKey());
						for (FileInformation fileInformation : entry.getValue()){
							System.out.println("Файл: " + fileInformation.getFileName() + " " + fileInformation.getFileSize());
						}
					}
				}
			}).start();

		}
	}
}