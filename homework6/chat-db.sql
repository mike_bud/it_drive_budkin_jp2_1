create table Users
(
    id         serial primary key,
    first_name varchar(20),
    last_name  varchar(20),
    email      varchar(20),
    password   integer
);


create table Chat
(
    id             serial primary key,
    title          varchar(20),
    date_of_create date,
    chat_id integer
);

create table message
(
    id        serial primary key,
    text      varchar(100),
    author    varchar(20),
    chat      integer,
    author_id integer,

    foreign key (author_id) references Users (id),
    foreign key (chat) references Chat (id)

);

create table User_chat
(
    author_id integer,
    chat_id    integer,
    foreign key (author_id) references Users (id),
    foreign key (chat_id) references Chat (id)
);