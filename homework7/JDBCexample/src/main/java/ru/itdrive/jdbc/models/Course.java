package ru.itdrive.jdbc.models;

import javafx.scene.chart.PieChart;

import java.util.Date;

public class Course {
    private Integer id;
    private String titale;
    private Date startDate;
    private Date endDate;

    public Course(Integer id, String titale, Date startDate, Date endDate) {
        this.id = id;
        this.titale = titale;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitale() {
        return titale;
    }

    public void setTitale(String titale) {
        this.titale = titale;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", titale=" + titale +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
