package ru.itdrive.jdbc;

import ru.itdrive.jdbc.models.Course;
import ru.itdrive.jdbc.models.Lesson;
import ru.itdrive.jdbc.models.Student;
import ru.itdrive.jdbc.repositories.course.CoursesRepository;
import ru.itdrive.jdbc.repositories.course.CoursesRepositoryImpl;
import ru.itdrive.jdbc.repositories.lesson.LessonsRepository;
import ru.itdrive.jdbc.repositories.lesson.LessonsRepositoryImpl;
import ru.itdrive.jdbc.repositories.student.StudentRepositories;
import ru.itdrive.jdbc.repositories.student.StudentRepositoriesImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

public class MainforRepositories {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "admin";

    public static void main(String[] args) throws Exception{

        Connection connection = DriverManager.getConnection(DB_URL,DB_USER,DB_PASSWORD); // подключение

        StudentRepositories repositories = new StudentRepositoriesImpl(connection);

        Student verona = repositories.find(4);
        System.out.println(verona);

        System.out.println("-------------------------");

        List<Student> students = repositories.findAll();
        System.out.println(students);

        System.out.println("-------------------------");

        CoursesRepository coursesRepository = new CoursesRepositoryImpl(connection);
        Course course = coursesRepository.findById(2);
        System.out.println(course);

        System.out.println("-------------------------");

        /*Course course1 = coursesRepository.findByTitale("MAth");
        System.out.println(course1);*/

        System.out.println("-------------------------");

        List<Course> course2 = coursesRepository.findAll();
        System.out.println(course2);

        System.out.println("-------------------------");

        LessonsRepository lessonsRepository = new LessonsRepositoryImpl(connection);
        Lesson lesson = lessonsRepository.find(1);
        System.out.println(lesson);

        System.out.println("-------------------------");

        List<Lesson> lessons = lessonsRepository.findAll();
        System.out.println(lessons);





    }
}
