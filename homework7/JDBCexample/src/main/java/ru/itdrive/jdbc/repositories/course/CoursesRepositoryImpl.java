package ru.itdrive.jdbc.repositories.course;

import ru.itdrive.jdbc.models.Course;
import ru.itdrive.jdbc.repositories.RowMapper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CoursesRepositoryImpl implements CoursesRepository {

    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from course where id = ";

    //language=SQL
    private static final String SQL_FIND_BY_Titale = "select * from course where titale = ";

    //language=SQL
    private static final String SQL_FIND_ALL = "select * from course";

    private  Connection connection;

    private RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
        public Course mapRow(ResultSet row) throws SQLException {
            return new Course(
                row.getInt("id"),
                row.getString("titale"),
                row.getDate("start_date"),
                row.getDate("end_date")
            );
        }
    };

    public CoursesRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    public Course findById(Integer id) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL_FIND_BY_ID + id);
        resultSet.next();
        return courseRowMapper.mapRow(resultSet);

    }

    public Course findByTitale(String titale) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL_FIND_BY_ID + titale);
        resultSet.next();
        return courseRowMapper.mapRow(resultSet);

    }

    public void save(Course object) {

    }

    public void update(Course object) {

    }

    public void delete(Integer id) {

    }

    public Course find(Integer id) throws SQLException {
        return null;
    }

    public List<Course> findAll() throws Exception {
        List<Course> courses = new ArrayList<Course>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL);
        while (resultSet.next()){
            Course course = courseRowMapper.mapRow(resultSet);
            courses.add(course);
        }
return courses;
    }
}
