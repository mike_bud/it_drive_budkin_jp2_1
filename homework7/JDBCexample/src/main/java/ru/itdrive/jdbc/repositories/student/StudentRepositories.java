package ru.itdrive.jdbc.repositories.student;

import ru.itdrive.jdbc.models.Student;
import ru.itdrive.jdbc.repositories.CrudRepositoriesJDBC;

public interface StudentRepositories extends CrudRepositoriesJDBC<Student> {
    Student findByFirstName (String firstName);
}
