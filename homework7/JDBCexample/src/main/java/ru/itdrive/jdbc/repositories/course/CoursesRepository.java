package ru.itdrive.jdbc.repositories.course;

import ru.itdrive.jdbc.models.Course;
import ru.itdrive.jdbc.repositories.CrudRepositoriesJDBC;

import java.sql.SQLException;

public interface CoursesRepository extends CrudRepositoriesJDBC<Course> {
    Course findById (Integer id) throws SQLException;

    Course findByTitale (String titale) throws SQLException;


}
