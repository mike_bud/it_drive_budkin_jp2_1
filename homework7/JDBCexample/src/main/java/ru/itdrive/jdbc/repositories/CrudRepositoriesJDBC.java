package ru.itdrive.jdbc.repositories;

import java.sql.SQLException;
import java.util.List;

public interface CrudRepositoriesJDBC <T>{
    void save (T object);
    void update(T object);
    void delete (Integer id);
    T find (Integer id) throws SQLException;

    List<T> findAll() throws Exception;
}
