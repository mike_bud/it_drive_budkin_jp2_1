package ru.itdrive.jdbc.repositories.student;

import ru.itdrive.jdbc.models.Student;
import ru.itdrive.jdbc.repositories.RowMapper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class StudentRepositoriesImpl implements StudentRepositories {

    //Language=SQL
    private static final String SQL_SELECT_ALL = "select * from student";

    //Language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = ";

    private Connection connection;

    private RowMapper<Student> studentRowMapper = new RowMapper<Student>() {
        public Student mapRow(ResultSet row) throws SQLException{
            return new Student(
                    row.getInt("id"),
                    row.getString("first_name"),
                    row.getString("last_name"),
                    row.getInt("age"),
                    row.getBoolean("is_active")
            );
        }
    };


    public StudentRepositoriesImpl(Connection connection) throws SQLException {
        this.connection = connection;
    }

    public Student findByFirstName(String firstName) {
        return null;
    }

    public void save(Student object) {

    }

    public void update(Student object) {

    }

    public void delete(Integer id) {

    }

    public Student find(Integer id) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
        resultSet.next();
        return studentRowMapper.mapRow(resultSet);

    }

    public List<Student> findAll() throws Exception{
        List<Student> result = new ArrayList<Student>();

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);

        while(resultSet.next()) {
            Student student = studentRowMapper.mapRow(resultSet);
            result.add(student);
        }
        return result;
    }

}
