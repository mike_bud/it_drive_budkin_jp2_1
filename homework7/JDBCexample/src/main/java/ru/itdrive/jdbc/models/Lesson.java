package ru.itdrive.jdbc.models;

public class Lesson {

    private Integer id;
    private String name;
    private Integer courseID;

    public Lesson(Integer id, String name, Integer courseID) {
        this.id = id;
        this.name = name;
        this.courseID = courseID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCourseID() {
        return courseID;
    }

    public void setCourseID(Integer courseID) {
        this.courseID = courseID;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", courseID=" + courseID +
                '}';
    }
}
