package ru.itdrive.jdbc.repositories.lesson;

import ru.itdrive.jdbc.models.Course;
import ru.itdrive.jdbc.models.Lesson;
import ru.itdrive.jdbc.repositories.RowMapper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LessonsRepositoryImpl implements LessonsRepository {

    private Connection connection;

    //Language=SQL
    private static final String SQL_FIND_BY_ID = "select * from lesson where id = ";

    //Language=SQL
    private static final String SQL_FIND_All = "select * from lesson";

    private RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getInt("course_id")
            );
        }
    };

    public LessonsRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    public Course findByName(String name) throws SQLException {
        return null;
    }

    public void save(Lesson object) {

    }

    public void update(Lesson object) {

    }

    public void delete(Integer id) {

    }

    public Lesson find(Integer id) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL_FIND_BY_ID + id);
        resultSet.next();
        return lessonRowMapper.mapRow(resultSet);

    }

    public List<Lesson> findAll() throws Exception {
        List<Lesson> lessons = new ArrayList<Lesson>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL_FIND_All);
        while (resultSet.next()){
            Lesson lesson = lessonRowMapper.mapRow(resultSet);
            lessons.add(lesson);
        }
        return lessons;
    }
}
