package ru.itdrive.jdbc.repositories.lesson;

import ru.itdrive.jdbc.models.Course;
import ru.itdrive.jdbc.models.Lesson;
import ru.itdrive.jdbc.repositories.CrudRepositoriesJDBC;

import java.sql.SQLException;

public interface LessonsRepository extends CrudRepositoriesJDBC<Lesson> {
    Course findByName (String name) throws SQLException;

}
